<h1>create new user</h1>
 <form method = 'post' action = "{{action('TodoController@update',$todo->id)}}">
 @csrf 
 @method('PATCH')
 <div class = "form-group">
 <label for = "title">todo to update</label>
 <input type = "text" class = "form-control" name = "title" value = "{{$todo->title}}">
 </div>
 
 <div class = "form-group">
     <input type = "submit" class = "form-control" name = "submit" value = "update" >
 </div>
 
 </form>
 
 <form method = 'post' action = "{{action('TodoController@destroy',$todo->id)}}">
 @csrf 
 @method('DELETE')
 <div class = "form-group">
     <input type = "submit" class = "form-control" name = "submit" value = "delete" >
 </div>