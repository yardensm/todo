<h1>This is your todo list</h1>
 
 <ul>
 
     @foreach($todos as $todo)
     <li>
         <a href ="{{route('todos.edit', $todo ->id)}}"> {{$todo->title}} </a>
     </li>
     @endforeach
 </ul> 
 <a href ="{{route('todos.create')}}"> create a new todo </a>